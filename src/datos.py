# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__="MichellHidalgo"
__date__ ="$24/06/2014 03:06:49 PM$"

from bs4 import BeautifulSoup
import urllib2

url = "http://ocw.mit.edu/courses/biology/7-343-biological-bases-of-learning-and-memory-spring-2014/lecture-summaries"
header = {'User-Agent': 'Mozilla/29.0'}
req = urllib2.Request(url,headers=header)
content = urllib2.urlopen(req)
soup = BeautifulSoup(content)
Semana = ""
Temas = ""
ResumenConferencia     = ""
datos1 = soup.find("table", { "class" : "tablewidth100" })
for row in datos1.findAll("tr"):
    row.string
    cells = row.findAll("td")
    if len(cells) == 3:
        Semana = cells[0].find(text=True)
        Temas = cells[1].find(text=True)
        lectura = cells[2].findAll("p")
        for x in range(len(lectura)):
            ResumenConferencia = lectura[x]
            print ResumenConferencia